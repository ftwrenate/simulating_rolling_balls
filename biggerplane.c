#include "biggerplane.h"
#include "math.h"
#include "iostream"


template <typename T>
inline
biggerPlane<T>::biggerPlane(const GMlib::DMatrix<GMlib::Vector<float, 3> > &c) {

    _c = c;


    this->_dm = GMlib::GM_DERIVATION_EXPLICIT;
}


template <typename T>
inline
biggerPlane<T>::biggerPlane( const biggerPlane<T>& copy ) {

    _c   = copy._c;

}


template <typename T>
biggerPlane<T>::~biggerPlane() {}



template <typename T>
void biggerPlane<T>::eval(float u, float v, int d1, int d2, bool /*lu*/, bool /*lv*/ ) {

    this->_p.setDim( d1+1, d2+1 );

    //((1-u)^4, 4u(1-u)^3,6u^2(1-u)^2, 4u^3(1-u) ,u^4)
    GMlib::DVector<T> u1(5);
    u1[0] = (1-u)*(1-u)*(1-u)*(1-u);
    u1[1] = 4*u*((1-u)*(1-u)*(1-u));
    u1[2] = 6*u*u*((1-u)*(1-u));
    u1[3] = 4*u*u*u*((1-u));
    u1[4] = u*u*u*u;


    //((1-v)^4, 4v(1-v)^3,6v^2(1-v)^2, 4v^3(1-v) ,v^4)
    GMlib::DVector<T> v1(5);
    v1[0] = (1-v)*(1-v)*(1-v)*(1-v);
    v1[1] = 4*v*((1-v)*(1-v)*(1-v));
    v1[2] = 6*v*v*((1-v)*(1-v));
    v1[3] = 4*v*v*v*((1-v));
    v1[4] = v*v*v*v;


    this->_p[0][0] = u1*(_c^v1);


    if( this->_dm == GMlib::GM_DERIVATION_EXPLICIT ) {

        // 1st
        //The derivative of:(((1-u)^4, 4u(1-u)^3,6u^2(1-u)^2, 4u^3(1-u) ,u^4)
        GMlib::DVector<T> u2(5);

        u2[0] = 4*((u-1)*(u-1)*(u-1));
        u2[1] = -4*(((u-1)*(u-1))*(4*u-1));
        u2[2] = 12*u*((u-1)*(2*u-1));
        u2[3] =  -4*u*u*(4*u-3);
        u2[4] = 4*u*u*u;



        if(d1){
            this->_p[1][0] = u2*(_c^v1); // S_u
        }
         //The derivative of:((1-v)^4, 4v(1-v)^3,6v^2(1-v)^2, 4v^3(1-v) ,v^4)
        GMlib::DVector<T> v2(5);
        v2[0] = 4*((v-1)*(v-1)*(v-1));
        v2[1] = -4*(((v-1)*(v-1))*(4*v-1));
        v2[2] = 12*v*((v-1)*(2*v-1));
        v2[3] =  -4*v*v*(4*v-3);
        v2[4] = 4*v*v*v;



        if(d2){
            this->_p[0][1] = u1*(_c^v2); // S_v
        }

        if(d1>1 && d2>1)  this->_p[1][1] = u2*(_c^v2); // S_uv

        // 2nd
        if(d1>1)          this->_p[2][0] = GMlib::Vector<float,3>(float(0)); // S_uu
        if(d2>1)          this->_p[0][2] = GMlib::Vector<float,3>(float(0)); // S_vv
        if(d1>1 && d2)    this->_p[2][1] = GMlib::Vector<float,3>(float(0)); // S_uuv
        if(d1   && d2>1)  this->_p[1][2] = GMlib::Vector<float,3>(float(0)); // S_uvv
        if(d1>1 && d2>1)  this->_p[2][2] = GMlib::Vector<float,3>(float(0)); // S_uuvv

        // 3rd
        if(d1>2)          this->_p[3][0] = GMlib::Vector<float,3>(float(0)); // S_uuu
        if(d2>2)          this->_p[0][3] = GMlib::Vector<float,3>(float(0)); // S_vvv
        if(d1>2 && d2)    this->_p[3][1] = GMlib::Vector<float,3>(float(0)); // S_uuuv
        if(d1   && d2>2)  this->_p[1][3] = GMlib::Vector<float,3>(float(0)); // S_uvvv
        if(d1>2 && d2>1)  this->_p[3][2] = GMlib::Vector<float,3>(float(0)); // S_uuuvv
        if(d1>1 && d2>2)  this->_p[2][3] = GMlib::Vector<float,3>(float(0)); // S_uuvvv
        if(d1>2 && d2>2)  this->_p[3][3] = GMlib::Vector<float,3>(float(0)); // S_uuuvvv

    }

    //std::cout<<"matp"<<this->_p<<std::endl;
}

template <typename T>
inline
float biggerPlane<T>::getEndPU(){

    return T(1);
}

template <typename T>
inline
float biggerPlane<T>::getEndPV() {

    return T(1);
}


template <typename T>
inline
float biggerPlane<T>::getStartPU() {

    return T(0);
}


template <typename T>
inline
float biggerPlane<T>::getStartPV() {

    return T(0);
}










