#include "controller.h"
#include "balls.h"
#include "walls.h"
#include <gmParametricsModule>
#include "biggerplane.h"
#include <stdlib.h>
#include <random>


using namespace GMlib;


template <typename T>
controller<T>::controller(){

    this->setSurroundingSphere(GMlib::Sphere<float,3>(100.0f));

    _color.push_back(GMlib::GMmaterial::BlackRubber);
    _color.push_back(GMlib::GMmaterial::Brass);
    _color.push_back(GMlib::GMmaterial::Chrome);
    _color.push_back(GMlib::GMmaterial::Emerald);
    _color.push_back(GMlib::GMmaterial::Obsidian);
    _color.push_back(GMlib::GMmaterial::Ruby);
    _color.push_back(GMlib::GMmaterial::Sapphire);
    _color.push_back(GMlib::GMmaterial::Silver);

    insertTerrain();
    insertBalls();
    insertWalls();
}
template <typename T>
void controller<T>::localSimulate(double dt) {

    //collDetectionWall(dt);
    collDetectionBall(dt);

    for(int i = 0; i < _balls.size(); i++)
    {
        _balls.at(i)->computeStep(dt);
    }

    //rotate( GMlib::Angle(90) * dt, GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f ) );
    //rotate( GMlib::Angle(180) * dt, GMlib::Vector<T,3>( 1.0f, 0.0f, 0.0f ) );
    //translate(GMlib::Vector<T,3>(0.1f,0,0));
}

//Find collision wall
template <typename T>
bool controller<T>::findCollWall(std::shared_ptr<balls<T>> b1, std::shared_ptr<GMlib::PPlane<float> > w1, double dt, T x, collObj<T> &c)
{

    // Gives a start position
    w1->estimateClpPar(b1->getDSPoint(),_u,_v);


    if (!w1->getClosestPoint(b1->getDSPoint(),_u,_v))
    {
        return false;
    }
    GMlib::DMatrix<GMlib::Vector<float,3>> myMatrix = w1->evaluateGlobal(_u,_v,1,1);

    GMlib::Vector<float,3> normal = myMatrix[1][0]^myMatrix[0][1];
    GMlib::Vector<T,3> n (normal(0),normal(1),normal(2));
    n.normalize();
    normal.normalize();

    GMlib::Point<T,3> clp( myMatrix[0][0](0),myMatrix[0][0](1),myMatrix[0][0](2));
    GMlib::Vector<T,3> distanceWallBall = clp - GMlib::Point<T,3>(b1->getGlobalPos()(0),b1->getGlobalPos()(1),b1->getGlobalPos()(2));
    float dn = distanceWallBall*normal;

    // The balls are inside the walls.
    if(dn + b1->getRadius() > 0.)
    {
        b1->translate(2.*(dn + b1->getRadius())*normal);
        dn -= 2.*(dn + b1->getRadius());
    }
    if(std::abs(b1->getDS() * n) > 1e-6)
    {
        x = (b1->getRadius() + (dn))/ (b1->getDS() * n);
        if(x > T(0) && x < T(1))
        {

            c = collObj<T>(b1,w1,x,n);
            std::cout<<"kollisjon: " <<n <<std::endl;

            return true;
        }
    }
    return false;
}

// Find collision ball
template <typename T>
bool controller<T>::findCollBall(std::shared_ptr<balls<T>> b1, std::shared_ptr<balls <T>> b2, double dt, T x, collObj<T> &cob)
{
    auto convFloat1 = b1->getGlobalPos();
    auto convFloat2 = b2->getGlobalPos();

    GMlib::Point<T,3> b1_getGlobalPos(convFloat1(0),convFloat1(1),convFloat1(2));
    GMlib::Point<T,3> b2_getGlobalPos(convFloat2(0),convFloat2(1),convFloat2(2));

    GMlib::Vector<T,3>  distanceBallBall = b1->getDS() - b2->getDS(); // ds
    GMlib::Vector<T,3> distancePositionBall = (b1_getGlobalPos - b2_getGlobalPos); //dp
    GMlib::Vector<float,3> distanceFloat (distancePositionBall(0),distancePositionBall(1),distancePositionBall(2));
    T ballRadius = (b1->getRadius() + b2->getRadius());// r

    T a = distanceBallBall * distanceBallBall;   // a = ds * ds
    T b = 2 *(distanceBallBall* distanceFloat); // b = 2 * (ds * dp)
    T c = (distanceFloat *distanceFloat) - (ballRadius * ballRadius); // c = dp*dp -r^2



   // So the balls doesnt go into eachother
   if(c < 0)
    {
       std::cout<<"c "<<c<<std::endl;
        float newPos = 0.51*(ballRadius - distanceFloat.getLength())/distanceFloat.getLength();

        b1->translate(newPos * distanceFloat);
        b2->translate(-newPos*distanceFloat);

        distanceFloat *= 1 +(2*newPos);
        b = 2*(distanceBallBall * distanceFloat);
        c = (distanceFloat *distanceFloat) - (ballRadius * ballRadius);
     }

    T numerator = (b*b)-4*a*c;

   if((numerator) > 0)
    {
        // ABC- formula
        x = (-b - std::sqrt((numerator)))/(2*a);

        if(x > T(0) && x < T(1))
        {

            cob = collObj<T>(b1,b2,x);
            std::cout<<"kollisjon ball: " <<std::endl;
            return true;
        }
    }
    return false;
}

template <typename T>
void controller<T>::handleCollision(collObj<T> obj, double dt)
{

    if(obj.getBall2())
    {
        obj.changeDirectionBallBall();
    }
    else
    {
        obj.getBall1()->setVelocity(obj.changeDirectionBallWall(dt));
        obj.getBall1()->setDS(obj.getNewDSBall1());

    }
}
template <typename T>
void controller<T>::collDetectionBall(T dt)
{
    Array<collObj<T>> coll;

    // For ball and ball
    for (int i=0; i< _balls.size(); i++)
    {
        for (int j = 1+i; j < _balls.size(); j++ )
        {
            collObj<T> obj;
            if(findCollBall(_balls[i],_balls[j], dt,0, obj))
                coll += obj;
        }
    }
    // For ball and wall
    for (int i=0; i< _balls.size(); i++)
    {
        for (int j = 0; j < _walls.size(); j++ )
        {
            collObj<T> obj;
            if(findCollWall(_balls[i],_walls[j], dt,0, obj))
                coll += obj;
        }
    }
    coll.sort();
    coll.makeUnique();
    while(coll.size() > 0)
    {

        collObj<T> cob = coll[0];
        coll.removeIndex(0);
        handleCollision(cob, dt);

        collObj<T> obj;

        // Detect coll for ball 1
       for( auto w : _walls )
            if(findCollWall(cob.getBall1(), w, dt,cob.getX(),obj))
                coll+=obj;

        for(auto b : _balls)
            if(cob.getBall1() != b)
                if(findCollBall(cob.getBall1(), b, dt,cob.getX(),obj))
                    coll+=obj;


        // Detect coll for ball 2
        if(cob.getBall2())
        {
            for( auto w : _walls )
                if(findCollWall(cob.getBall1(), w, dt,cob.getX(),obj))
                    coll+=obj;

            for(auto b : _balls)
                if(cob.getBall2() != b)
                    if(findCollBall(cob.getBall2(), b, dt,cob.getX(),obj))
                        coll+=obj;

        }
        coll.sort();
        coll.makeUnique();

    }
}

//-----------------------------------------------------------------------------------
//
//                                  ***BALLS***
//
//-----------------------------------------------------------------------------------
template <typename T>
void controller<T>::insertBalls()
{

    auto ball = std::make_shared<balls<T>>(6, 6,GMlib::Vector<T,3>(20.0f,0.0f,0.0f));
    ball->toggleDefaultVisualizer();
    ball->translate(GMlib::Vector<float,3>(10.0f,0.0f,0.0f));
    ball->replot(200,200,1,1);
    ball->setMaterial(GMlib::GMmaterial::Ruby);
    _balls.push_back(ball);
    this->insert(ball.get());
    ball->setTerrain(_terrain);


    auto ball2 = std::make_shared<balls<T>>(4, 4,GMlib::Vector<T,3>(20.0f,0.0f,0.0f));
    ball2->toggleDefaultVisualizer();
    ball2->translate(GMlib::Vector<float,3>(20.0f,10.0f,0.0f));
    ball2->replot(200,200,1,1);
    ball2->setMaterial(GMlib::GMmaterial::Emerald);
    _balls.push_back(ball2);
    this->insert(ball2.get());
    ball2->setTerrain(_terrain);


     auto ball3 = std::make_shared<balls<T>>(3, 3,GMlib::Vector<T,3>(-10.0f,7.0f,0.0f));
    ball3->toggleDefaultVisualizer();
    ball3->translate(GMlib::Vector<float,3>(-15.0f,0.0f,0.0f));
    ball3->replot(200,200,1,1);
    ball3->setMaterial(GMlib::GMmaterial::Sapphire);
    _balls.push_back(ball3);
    this->insert(ball3.get());
    ball3->setTerrain(_terrain);

    auto ball4 = std::make_shared<balls<T>>(3, 3,GMlib::Vector<T,3>(20.0f,0.0f,0.0f));
    ball4->toggleDefaultVisualizer();
    ball4->translate(GMlib::Vector<float,3>(-10.0f,0.0f,0.0f));
    ball4->replot(200,200,1,1);
    ball4->setMaterial(GMlib::GMmaterial::Ruby);
    _balls.push_back(ball4);
    this->insert(ball4.get());
    ball4->setTerrain(_terrain);


    auto ball5 = std::make_shared<balls<T>>(5, 5,GMlib::Vector<T,3>(20.0f,0.0f,0.0f));
    ball5->toggleDefaultVisualizer();
    ball5->translate(GMlib::Vector<float,3>(30.0f,10.0f,0.0f));
    ball5->replot(200,200,1,1);
    ball5->setMaterial(GMlib::GMmaterial::Emerald);
    _balls.push_back(ball5);
    this->insert(ball5.get());
    ball5->setTerrain(_terrain);


     auto ball6 = std::make_shared<balls<T>>(5, 5,GMlib::Vector<T,3>(-10.0f,7.0f,0.0f));
    ball6->toggleDefaultVisualizer();
    ball6->translate(GMlib::Vector<float,3>(-20.0f,0.0f,0.0f));
    ball6->replot(200,200,1,1);
    ball6->setMaterial(GMlib::GMmaterial::Sapphire);
    _balls.push_back(ball6);
    this->insert(ball6.get());
    ball6->setTerrain(_terrain);

    auto ball7 = std::make_shared<balls<T>>(4, 4,GMlib::Vector<T,3>(20.0f,0.0f,0.0f));
    ball7->toggleDefaultVisualizer();
    ball7->translate(GMlib::Vector<float,3>(0.0f,15.0f,0.0f));
    ball7->replot(200,200,1,1);
    ball7->setMaterial(GMlib::GMmaterial::Ruby);
    _balls.push_back(ball7);
    this->insert(ball7.get());
    ball7->setTerrain(_terrain);


    auto ball8 = std::make_shared<balls<T>>(3, 3,GMlib::Vector<T,3>(20.0f,0.0f,0.0f));
    ball8->toggleDefaultVisualizer();
    ball8->translate(GMlib::Vector<float,3>(10.0f,20.0f,0.0f));
    ball8->replot(200,200,1,1);
    ball8->setMaterial(GMlib::GMmaterial::Emerald);
    _balls.push_back(ball8);
    this->insert(ball8.get());
    ball8->setTerrain(_terrain);


     auto ball9 = std::make_shared<balls<T>>(6, 6,GMlib::Vector<T,3>(-10.0f,7.0f,0.0f));
    ball9->toggleDefaultVisualizer();
    ball9->translate(GMlib::Vector<float,3>(-30.0f,20.0f,0.0f));
    ball9->replot(200,200,1,1);
    ball9->setMaterial(GMlib::GMmaterial::Sapphire);
    _balls.push_back(ball9);
    this->insert(ball9.get());
    ball9->setTerrain(_terrain);

}
//-----------------------------------------------------------------------------------
//
//                           ***WALLS FOR THE TERRAIN***
//
//-----------------------------------------------------------------------------------
template <typename T>
void controller<T>::insertWalls()
{
    // A 3x3 Matrix. Front Wall
    /*GMlib::DMatrix<GMlib::Vector<float,3>> wallsVecFront(3,3);
    wallsVecFront[0][0] = GMlib::Vector<float,3>(-55,0,20);
    wallsVecFront[0][1] = GMlib::Vector<float,3>(0,0,20);
    wallsVecFront[0][2] = GMlib::Vector<float,3>(55,0,20);

    wallsVecFront[1][0] = GMlib::Vector<float,3>(-55,0,2);
    wallsVecFront[1][1] = GMlib::Vector<float,3>(0,0,2);
    wallsVecFront[1][2] = GMlib::Vector<float,3>(55,0,2);

    wallsVecFront[2][0] = GMlib::Vector<float,3>(-50,-5,1);
    wallsVecFront[2][1] = GMlib::Vector<float,3>(0,-5,1);
    wallsVecFront[2][2] = GMlib::Vector<float,3>(50,-5,1);

    auto plane_n_visu = new GMlib::PSurfNormalsVisualizer<T,3>;
    auto WALLS_VEC_FRONT = std::make_shared<walls<T>>(wallsVecFront);
    WALLS_VEC_FRONT->insertVisualizer(plane_n_visu);

    WALLS_VEC_FRONT->toggleDefaultVisualizer();
    WALLS_VEC_FRONT->translate(GMlib::Vector<float,3>(0,55.0f,-1.0f));
    WALLS_VEC_FRONT->replot(50,50,1,1);
    _walls.push_back(WALLS_VEC_FRONT);
    this->insert(WALLS_VEC_FRONT.get());

    //-----------------------------------------------------------------------------------

    // A 3x3 Matrix. Back Wall
     GMlib::DMatrix<GMlib::Vector<float,3>> wallsVecBack(3,3);
    wallsVecBack[0][0] = GMlib::Vector<float,3>(-55,0,20);
    wallsVecBack[0][1] = GMlib::Vector<float,3>(0,0,20);
    wallsVecBack[0][2] = GMlib::Vector<float,3>(55,0,20);

    wallsVecBack[1][0] = GMlib::Vector<float,3>(-55,0,2);
    wallsVecBack[1][1] = GMlib::Vector<float,3>(0,0,2);
    wallsVecBack[1][2] = GMlib::Vector<float,3>(55,0,2);

    wallsVecBack[2][0] = GMlib::Vector<float,3>(-50,5,1);
    wallsVecBack[2][1] = GMlib::Vector<float,3>(0,5,1);
    wallsVecBack[2][2] = GMlib::Vector<float,3>(50,5,1);

    auto plane_n_visu_back = new GMlib::PSurfNormalsVisualizer<T,3>;
    auto WALLS_VEC_BACK = std::make_shared<walls<T>>(wallsVecBack);
    WALLS_VEC_BACK->insertVisualizer(plane_n_visu_back);
    WALLS_VEC_BACK->toggleDefaultVisualizer();
    WALLS_VEC_BACK->translate(GMlib::Vector<float,3>(0,-55.0f,-1.0f));
    WALLS_VEC_BACK->replot(50,50,1,1);
     _walls.push_back(WALLS_VEC_BACK);
    this->insert(WALLS_VEC_BACK.get());
    //-----------------------------------------------------------------------------------

    // A 3x3 Matrix. Right Wall
     GMlib::DMatrix<GMlib::Vector<float,3>> wallsVecRight(3,3);
    wallsVecRight[0][0] = GMlib::Vector<float,3>(-55,0,20);
    wallsVecRight[0][1] = GMlib::Vector<float,3>(0,0,20);
    wallsVecRight[0][2] = GMlib::Vector<float,3>(55,0,20);

    wallsVecRight[1][0] = GMlib::Vector<float,3>(-55,0,2);
    wallsVecRight[1][1] = GMlib::Vector<float,3>(0,0,2);
    wallsVecRight[1][2] = GMlib::Vector<float,3>(55,0,2);

    wallsVecRight[2][0] = GMlib::Vector<float,3>(-50,5,1);
    wallsVecRight[2][1] = GMlib::Vector<float,3>(0,5,1);
    wallsVecRight[2][2] = GMlib::Vector<float,3>(50,5,1);

    auto plane_n_visu_back_right = new GMlib::PSurfNormalsVisualizer<T,3>;
    auto WALLS_VEC_RIGHT = std::make_shared<walls<T>>(wallsVecRight);
    WALLS_VEC_RIGHT->insertVisualizer(plane_n_visu_back_right);
    WALLS_VEC_RIGHT->toggleDefaultVisualizer();
    WALLS_VEC_RIGHT->rotate(GMlib::Angle(90), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f ));
    WALLS_VEC_RIGHT->translate(GMlib::Vector<float,3>(0,-55.0f,-1.0f));
    WALLS_VEC_RIGHT->replot(50,50,1,1);
    _walls.push_back(WALLS_VEC_RIGHT);
    this->insert(WALLS_VEC_RIGHT.get());


    // A 3x3 Matrix. Left Wall
     GMlib::DMatrix<GMlib::Vector<float,3>> wallsVecLeft(3,3);
    wallsVecLeft[0][0] = GMlib::Vector<float,3>(-55,0,20);
    wallsVecLeft[0][1] = GMlib::Vector<float,3>(0,0,20);
    wallsVecLeft[0][2] = GMlib::Vector<float,3>(55,0,20);

    wallsVecLeft[1][0] = GMlib::Vector<float,3>(-55,0,2);
    wallsVecLeft[1][1] = GMlib::Vector<float,3>(0,0,2);
    wallsVecLeft[1][2] = GMlib::Vector<float,3>(55,0,2);

    wallsVecLeft[2][0] = GMlib::Vector<float,3>(-50,-5,1);
    wallsVecLeft[2][1] = GMlib::Vector<float,3>(0,-5,1);
    wallsVecLeft[2][2] = GMlib::Vector<float,3>(50,-5,1);

    auto plane_n_visu_back_left = new GMlib::PSurfNormalsVisualizer<T,3>;
    auto WALLS_VEC_LEFT = std::make_shared<walls<T>>(wallsVecLeft);
    WALLS_VEC_LEFT->insertVisualizer(plane_n_visu_back_left);
    WALLS_VEC_LEFT->toggleDefaultVisualizer();
    WALLS_VEC_LEFT->rotate(GMlib::Angle(90), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f ));
    WALLS_VEC_LEFT->translate(GMlib::Vector<float,3>(0,55.0f,-1.0f));
    WALLS_VEC_LEFT->replot(50,50,1,1);
     _walls.push_back(WALLS_VEC_LEFT);
    this->insert(WALLS_VEC_LEFT.get());*/



    auto plane_n_visu = new GMlib::PSurfNormalsVisualizer<T,3>;
    auto plane =  std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float,3>(50,-50,0),GMlib::Vector<float,3>(0,0,50),GMlib::Vector<float,3>(0,100,0));
    plane->insertVisualizer(plane_n_visu);
    plane->toggleDefaultVisualizer();
    //plane->rotate(GMlib::Angle(90), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f ));
    //plane->translate(GMlib::Vector<float,3>(0,-55.0f,-1.0f));
    //WALLS_VEC->insertVisualizer(surface_visualizer);
    plane->replot(2,2,1,1);
    _walls.push_back(plane);
    this->insert(plane.get());


    auto plane_n_visu_left = new GMlib::PSurfNormalsVisualizer<T,3>;
    auto plane_left = std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float,3>(-50,-50,0),GMlib::Vector<float,3>(0,0,50),GMlib::Vector<float,3>(0,100,0));
    plane_left->insertVisualizer(plane_n_visu_left);
    plane_left->toggleDefaultVisualizer();
    plane_left->rotate(GMlib::Angle(180), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f ));
    plane_left->translate(GMlib::Vector<float,3>(100.0,0.0,0.0));
    //plane->rotate(GMlib::Angle(90), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f ));
    //plane->translate(GMlib::Vector<float,3>(0,-55.0f,-1.0f));
    //WALLS_VEC->insertVisualizer(surface_visualizer);
    plane_left->replot(2,2,1,1);
    _walls.push_back(plane_left);
    this->insert(plane_left.get());



    auto plane_n_visu_front = new GMlib::PSurfNormalsVisualizer<T,3>;
    auto plane_front =  std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float,3>(-50,-50,0),GMlib::Vector<float,3>(0,0,50),GMlib::Vector<float,3>(0,100,0));
    plane_front->insertVisualizer(plane_n_visu_front);
    plane_front->toggleDefaultVisualizer();
    plane_front->rotate(GMlib::Angle(90), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f ));
    plane_front->translate(GMlib::Vector<float,3>(100.0f,0.0f,0.0f));
    //plane->rotate(GMlib::Angle(90), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f ));
    //plane->translate(GMlib::Vector<float,3>(0,-55.0f,-1.0f));
    //WALLS_VEC->insertVisualizer(surface_visualizer);
    plane_front->replot(2,2,1,1);
    _walls.push_back(plane_front);
    this->insert(plane_front.get());


    auto plane_n_visu_back = new GMlib::PSurfNormalsVisualizer<T,3>;
    auto plane_back =  std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float,3>(-50,-50,0),GMlib::Vector<float,3>(0,0,50),GMlib::Vector<float,3>(0,100,0));
    plane_back->insertVisualizer(plane_n_visu_back);
    plane_back->toggleDefaultVisualizer();
    plane_back->rotate(GMlib::Angle(270), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f ));
    plane_back->translate(GMlib::Vector<float,3>(100.0f,0.0f,0.0f));
    //plane->rotate(GMlib::Angle(90), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f ));
    //plane->translate(GMlib::Vector<float,3>(0,-55.0f,-1.0f));
    //WALLS_VEC->insertVisualizer(surface_visualizer);
    plane_back->replot(2,2,1,1);
    _walls.push_back(plane_back);
    this->insert(plane_back.get());


    //-----------------------------------------------------------------------------------


}
//-----------------------------------------------------------------------------------
//
//                                  ***TERRAIN***
//
//-----------------------------------------------------------------------------------
template <typename T>
void controller<T>::insertTerrain()
{
    // A 5x5 Matrix
    GMlib::DMatrix<GMlib::Vector<float,3>> terrainVec(5,5);
    terrainVec[0][0] = GMlib::Vector<float,3>(-50,50,20); //1
    terrainVec[0][1] = GMlib::Vector<float,3>(-30,50,20);//2
    terrainVec[0][2] = GMlib::Vector<float,3>( 0,50,50);//3 ...50
    terrainVec[0][3] = GMlib::Vector<float,3>(30,50,0);//4
    terrainVec[0][4] = GMlib::Vector<float,3>(50,50,0);//5 ...50

    terrainVec[1][0] = GMlib::Vector<float,3>(-50,30,0);//6
    terrainVec[1][1] = GMlib::Vector<float,3>(-30,30,0);//7
    terrainVec[1][2] = GMlib::Vector<float,3>(0,30,0);//8
    terrainVec[1][3] = GMlib::Vector<float,3>(30,30,0);//9
    terrainVec[1][4] = GMlib::Vector<float,3>(50,30,0);//10

    terrainVec[2][0] = GMlib::Vector<float,3>(-50,0,0);//11
    terrainVec[2][1] = GMlib::Vector<float,3>(-30,0,0);//12
    terrainVec[2][2] = GMlib::Vector<float,3>(0,0,0);//13
    terrainVec[2][3] = GMlib::Vector<float,3>(30,0,0);//14
    terrainVec[2][4] = GMlib::Vector<float,3>(50,0,0);//15

    terrainVec[3][0] = GMlib::Vector<float,3>(-50,-30,0);//16
    terrainVec[3][1] = GMlib::Vector<float,3>(-30,-30,0);//17
    terrainVec[3][2] = GMlib::Vector<float,3>(0,-30,0);//18
    terrainVec[3][3] = GMlib::Vector<float,3>(30,-30,0);//19
    terrainVec[3][4] = GMlib::Vector<float,3>(50,-30,0);//20

    terrainVec[4][0] = GMlib::Vector<float,3>(-50,-50,0);//21
    terrainVec[4][1] = GMlib::Vector<float,3>(-30,-50,0);//22
    terrainVec[4][2] = GMlib::Vector<float,3>(0,-50,0);//23
    terrainVec[4][3] = GMlib::Vector<float,3>(30,-50,0);//24
    terrainVec[4][4] = GMlib::Vector<float,3>(50,-50,0);//25

    _terrain = std::make_shared<biggerPlane<T>>(terrainVec);

    _terrain->toggleDefaultVisualizer();
   // TERRAIN_MAT_VEC->rotate(GMlib::Angle(90), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f ));
    //TERRAIN_MAT_VEC->translate(GMlib::Vector<float,3>(0,55.0f,-1.0f));
    //WALLS_VEC->insertVisualizer(surface_visualizer);
    _terrain->setMaterial(GMlib::GMmaterial::PolishedBronze);

    _terrain->replot(30,30,1,1);
     //wallArr.push_back(TERRAIN_MAT_VEC);
    this->insert(_terrain.get());
   /* TERRAIN_MAT_VEC =  std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float,3>(-50,-50,0),GMlib::Vector<float,3>(100,0,0),GMlib::Vector<float,3>(0,100,0));
    TERRAIN_MAT_VEC->toggleDefaultVisualizer();
    //SURFACE_TEST_MAT_VEC->insertVisualizer(surface_visualizer);
    TERRAIN_MAT_VEC->replot(2,2,1,1);

    this->insert(TERRAIN_MAT_VEC.get());*/

}
