#ifndef COLLOBJ_H
#define COLLOBJ_H
#include "walls.h"
#include "balls.h"
#include <cmath>
#include <parametrics/gmpplane>

template <typename T>
class collObj
{

public:

    collObj(){}
    collObj(std::shared_ptr<balls <T>> b1, std::shared_ptr<GMlib::PPlane <float>> w1, T x, GMlib::Vector<T,3> wallNormal);
    collObj(std::shared_ptr<balls <T>> b1, std::shared_ptr<balls <T>> b2, T x);

    bool operator<(const collObj<T> &other) const;
    bool operator==(const collObj<T> &other) const;
    std::shared_ptr<GMlib::PPlane<float> > getWall() const;

    std::shared_ptr<balls <T>> getBall1() const;
    std::shared_ptr<balls <T>> getBall2() const;
    void setX(T x);
    void setBall1( std::shared_ptr<balls <T>> b1);
    void setBall2( std::shared_ptr<balls <T>> b2);
    void changeDirectionBallBall();
    GMlib::Vector<T,3> getNewDSBall1();
    GMlib::Vector<T,3> getNewDSBall2();
    GMlib::Vector<T,3> changeDirectionBallWall(double dt);

    T getX() const;


private:
    T _x;
    GMlib::Vector<T,3> _wallNormal;
    std::shared_ptr<balls <T>> _ball1;
    std::shared_ptr<balls <T>> _ball2;
    std::shared_ptr<GMlib::PPlane <float>> _walls;
    GMlib::Vector<T,3> _newVelocityBall1;
    GMlib::Vector<T,3> _newVelocityBall2;
    GMlib::Vector<T,3> _newDSBall1;
    GMlib::Vector<T,3> _newDSBall2;
   //std::shared_ptr<walls <T>> _walls;

};
#include "collobj.c"
#endif // COLLOBJ_H
