#ifndef WALLS_H
#define WALLS_H
#include "../gmlib/modules/parametrics/src/gmpsurf.h"


template <typename T>
class walls : public GMlib::PSurf<float,3>{
    GM_SCENEOBJECT(walls)
   public:
     walls(const GMlib::DMatrix<GMlib::Vector<float, 3>> &c);
    walls( const walls<T>& copy );
    virtual ~walls();



   protected:
    GMlib::DMatrix<GMlib::Vector<float,3>> _c;

     void                          eval(float u, float v, int d1, int d2, bool lu = true, bool lv = true );
     float                         getEndPU();
     float                         getEndPV();
     float                         getStartPU();
     float                         getStartPV();


};
#include "walls.cpp"
#endif // WALLS_H
