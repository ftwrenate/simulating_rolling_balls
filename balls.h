#ifndef BALLS_H
#define BALLS_H


#include <parametrics/gmpsphere>
#include <stdlib.h>
#include "biggerplane.h"
#include <parametrics/gmpplane>

template <typename T>
class balls : public GMlib::PSphere<float> {

public:

    balls(T size, T mass,GMlib::Vector<T,3> velocity = GMlib::Vector<T,3>()):GMlib::PSphere<float>(float(size))
    {
        this->_mass = mass;
        this->_velocity = velocity;
        _gravity = GMlib::Vector<T,3>(0.0f,0.0f,-9.81f);

        _ds = GMlib::Vector<T,3>(T(0));
    }
    T getSize() const;
    T getMass() const;
    GMlib::Vector<T,3> getDS() const;
    GMlib::Point<float,3> getDSPoint() const;
    void setDSPoint(GMlib::Point<float,3> p1);
    GMlib::Vector<T,3> getVelocity() const;
    GMlib::Vector<float,3> getGravity() const;
    void setDS(GMlib::Vector<T,3> ds);
    void setVelocity(GMlib::Vector<T,3> velocity);
    void computeStep(T dt);
    void setTerrain(std::shared_ptr<biggerPlane<T>> terrain);

protected:
    void localSimulate(double dt);

private:
    T _size;
    T _mass;
    GMlib::Point<float,3> _p1;
    float _u;
    float _v;
    GMlib::Vector<T,3> _gravity;
    GMlib::Vector<T,3> _velocity;
    GMlib::Vector<T,3> _ds;
    GMlib::UnitVector<T,3> _normal;
    std::shared_ptr<biggerPlane<T>> _terrain;

}; // END class Balls
#include "balls.c"
#endif // BALLS_H

