#include "collobj.h"

// Constructor for ball and wall
template <typename T>
collObj<T>::collObj(std::shared_ptr<balls <T>> b1, std::shared_ptr<GMlib::PPlane<float> > w1, T x, GMlib::Vector<T,3> wallNormal)
{
    _ball1 = b1;
    _walls = w1;
    _wallNormal= wallNormal;
    _x = x;
    _ball2 = nullptr;
}
// Constructor for ball and ball
template <typename T>
collObj<T>::collObj(std::shared_ptr<balls <T>> b1, std::shared_ptr<balls <T>> b2, T x)
{
    _ball1 = b1;
    _ball2 = b2;
    _x = x;
}

template <typename T>
std::shared_ptr<GMlib::PPlane <float>> collObj<T>::getWall() const
{
    return _walls;
}
template <typename T>
T collObj<T>:: getX() const
{
    return _x;
}
template <typename T>
void collObj<T>::setX(T x)
{
    _x = x;
}
// For sort in controller.c
template <typename T>
bool collObj<T>::operator<(const collObj<T> &other) const
{
    return _x < other.getX();
}

// For Make_unique in controller.c
template <typename T>
bool collObj<T>::operator==(const collObj<T> &other) const
{
    if(_ball1 == other.getBall1())
    {
        return true;
    }
    if(_ball1 == other.getBall2())
    {
        return true;
    }
    if(_ball2 == other.getBall1())
    {
        return true;
    }
    if(_ball2 == other.getBall2())
    {
        return true;
    }
    return false;
}
template <typename T>
std::shared_ptr<balls <T>> collObj<T>::getBall1() const
{
    return _ball1;
}
template <typename T>
std::shared_ptr<balls <T>> collObj<T>::getBall2() const
{
    return _ball2;
}
template <typename T>
void collObj<T>::setBall1( std::shared_ptr<balls <T>> b1)
{
    _ball1 = b1;
}
template <typename T>
void collObj<T>::setBall2( std::shared_ptr<balls <T>> b2)
{
    _ball2 = b2;
}
template <typename T>
GMlib::Vector<T,3> collObj<T>::getNewDSBall1()
{
    return _newDSBall1;
}
template <typename T>
GMlib::Vector<T,3> collObj<T>::getNewDSBall2()
{
    return _newDSBall2;
}
// Change direction when a ball collides with a wall
template <typename T>
GMlib::Vector<T, 3> collObj<T>::changeDirectionBallWall(double dt)
{

    // Reflection vector formula: r = d - 2(d*n)*n
    _newVelocityBall1 = _ball1->getVelocity() - 2*(_ball1->getVelocity()*_wallNormal)*_wallNormal;

    _newDSBall1 = _ball1->getDS() * getX() *dt + (1-getX()) * dt *_newVelocityBall1;

    return _newVelocityBall1;
}
// Change direction when two balls collide
template <typename T>
void collObj<T>::changeDirectionBallBall() {

    GMlib::Vector<T,3> v1;
    GMlib::Vector<T,3> v2;
    GMlib::Vector<T,3> u1 = _ball1->getVelocity();
    GMlib::Vector<T,3> u2 = _ball2->getVelocity();

    GMlib::Point<float,3> x1 = _ball1->getGlobalPos();
    GMlib::Point<float,3> x2 = _ball2->getGlobalPos();
    T m1 = _ball1->getMass();
    T m2 = _ball2 ->getMass();


    // Elastic collision formula
    _newVelocityBall1 = u1 - (2*m2/(m1+m2)) * (((u1-u2)*(x1-x2))/((x1-x2)* (x1-x2))) *(x1-x2);
    _newVelocityBall2 = u2 -(2*m1/(m1+m2)) * (((u2-u1)*(x2-x1))/((x2-x1)*(x2-x1)))*(x2-x1);


    v1 = _newVelocityBall1;
    v2 = _newVelocityBall2;


    _ball1->setVelocity(v1);
    _ball2->setVelocity(v2);
    _ball1->setDS(_newDSBall1);
    _ball2->setDS(_newDSBall2);
}
