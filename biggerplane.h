#ifndef BIGGERPLANE_H
#define BIGGERPLANE_H
#include "../gmlib/modules/parametrics/src/gmpsurf.h"

template <typename T>
  class biggerPlane : public GMlib::PSurf<float,3> {
   GM_SCENEOBJECT(biggerPlane)
  public:
    biggerPlane( const GMlib::DMatrix<GMlib::Vector<float,3>>& c);
   biggerPlane( const biggerPlane<T>& copy );
   virtual ~biggerPlane();



  protected:
   GMlib::DMatrix<GMlib::Vector<float,3>> _c;

    void                      eval(float u, float v, int d1, int d2, bool lu = true, bool lv = true );
    float                     getEndPU();
    float                     getEndPV();
    float                     getStartPU();
    float                     getStartPV();


};
#include "biggerplane.c"
#endif // BIGGERPLANE_H
