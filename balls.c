#include "balls.h"

template <typename T>
GMlib::Vector<T,3> balls<T>:: getVelocity() const
{
    return _velocity;
}
template <typename T>
GMlib::Vector<float,3> balls<T>::getGravity() const
{
    return _gravity;
}

template <typename T>
T balls<T>:: getSize() const
{
    return _size;
}
template <typename T>
T balls<T>:: getMass() const
{
    return _mass;
}
template <typename T>
GMlib::Vector<T,3> balls<T>::getDS() const
{
    return _ds;
}
template <typename T>
GMlib::Point<float, 3> balls<T>::getDSPoint() const
{
    return _p1;
}
template <typename T>
void balls<T>::setDSPoint(GMlib::Point<float, 3> p1)
{
    _p1 = p1;
}
template <typename T>
void balls<T>::setVelocity(GMlib::Vector<T,3> velocity)
{
    _velocity = velocity;
}
template <typename T>
void balls<T>::setDS(GMlib::Vector<T,3> ds)
{
    _ds = ds;
}

template <typename T>
void balls<T>::computeStep(T dt)
{

    // auto p1_ggp = this->getGlobalPos();
    _ds = dt * _velocity + 1/2 *(dt*dt)*_gravity;

    // GMlib::Point<T,3> p1_getGlobalPos(p1_ggp(0),p1_ggp(1),p1_ggp(2));

    GMlib::Vector<float,3> ds(_ds(0),_ds(1),_ds(2));
    //GMlib::Point<float,3> p1 = this->getGlobalPos() + ds;
     setDSPoint(this->getGlobalPos() + ds);
    _terrain->estimateClpPar(_p1,_u,_v);
    _terrain->getClosestPoint(_p1,_u,_v);


    GMlib::DMatrix<GMlib::Vector<float,3>> myMatrix = _terrain->evaluateGlobal(_u,_v,1,1); // Closest point to terrain + derivate.

    _normal = myMatrix[1][0]^myMatrix[0][1];
    _normal.normalize();
    ds = myMatrix[0][0] + this->_radius * _normal - this->getGlobalPos();

    _ds = GMlib::Vector<T,3>(ds(0),ds(1),ds(2));
    _velocity += dt * _gravity;

    _velocity -= (_normal * _velocity) * _normal;

}
template <typename T>
void balls<T>:: localSimulate(double dt) {



    // En for fart



    // std::cout<<"DS: "<<_ds<<std::endl;


    GMlib::Vector<float,3> _dsStep(_ds[0],_ds[1],_ds[2]);
    GMlib::Vector<float,3> n (_normal(0),_normal(1),_normal(2));

    float rot = (_dsStep.getLength() / (M_2PI*this->getRadius()))*M_2PI;
    GMlib::Vector<float,3> rotAxis =n^_dsStep;
    this->translateGlobal(_dsStep);
    this->rotateGlobal(GMlib::Angle(rot), rotAxis);
    //this->rotateGlobal( GMlib::Angle(180) * dt, GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ) );


}
template <typename T>
void balls<T>::setTerrain(std::shared_ptr<biggerPlane<T> > terrain)
{
    _terrain = terrain;
}
