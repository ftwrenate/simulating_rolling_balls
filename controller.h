#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <scene/gmsceneobject.h>
#include "balls.h"
#include "walls.h"
#include "vector"
#include <memory>
#include "collobj.h"
#include <parametrics/gmpplane>


using namespace GMlib;

template <typename T>
class controller : public SceneObject {
    GM_SCENEOBJECT(controller)

    public:

        controller();
    void insertBalls();
    void insertWalls();
    void insertTerrain();
    void handleCollision(collObj<T> obj, double dt);
  //  void collDetectionWall(T dt);
    void collDetectionBall(T dt);
    bool findCollWall(std::shared_ptr<balls<T>> b1, std::shared_ptr<GMlib::PPlane <float>> w1, double dt, T x, collObj<T> &c);
    bool findCollBall(std::shared_ptr<balls<T>> b1, std::shared_ptr<balls <T>> b2, double dt, T x, collObj<T> &cob);

protected:
    void localSimulate(double dt);

private:
    float _u;
    float _v;
    std::vector<std::shared_ptr<balls <T>>> _balls;
    std::vector<GMlib::Material> _color;
     std::shared_ptr<biggerPlane <T>> _terrain;
    //std::shared_ptr<GMlib::PPlane<float>> TERRAIN_MAT_VEC;
   // std::vector<std::shared_ptr<walls <T>>> _walls;
    std::vector<std::shared_ptr<GMlib::PPlane <float>>> _walls;
    // std::vector<std::shared_ptr<collObj<T>>> coll;


};
#include "controller.c"
#endif // CONTROLLER_H
